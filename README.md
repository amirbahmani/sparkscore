# README #

The  method  of  the  efficient  score  statistic  is
used  extensively  to  conduct  inference  for  high  throughput
genomic  data  due  to  its  computational  efficiency  and  ability
to  accommodate  simple  and  complex  phenotypes.  Inference
based  on  these  statistics  can  readily  incorporate
a  priori knowledge from a vast collection of bioinformatics databases
to  further  refine  the  analyses.  The  sampling  distribution  of
the  efficient  score  statistic  is  typically  approximated  using
asymptotics.  As  this  may  be  inappropriate  in  the  context  of
small  study  size,  or  uncommon  or  rare  variants,  resampling
methods  are  often  used  to  approximate  the  exact  sampling
distribution.  We  propose  SparkScore,  a  set  of  distributed
computational  algorithms  implemented  in  Apache  Spark,  to
leverage the embarrassingly parallel nature of genomic resampling 
inference  on  the  basis  of  the  efficient  score  statistics.
We  illustrate  the  application  of  this  computational  approach
for  the  analysis  of  data  from  genome-wide  analysis  studies
(GWAS).  This  computational  approach  also  harnesses  the
fault-tolerant features of Spark and can be readily extended to
analysis of DNA and RNA sequencing data, including expression quantitative 
trait loci (eQTL) and phenotype association
studies.

* Version 0.1.0

### How do I get set up? ###
Install Apache Ant 

Install Maven

Install Apache Spark

Here is a log file of the build process:
https://bitbucket.org/amirbahmani/sparkscore/src/c82cb4d70ce059ecc21fcf61612c20b3fd8a3844/Java/build.log?at=master&fileviewer=file-view-default

### Execution ###
Use preprocessing.sh to generate input sample datasets


echo "Exec: 47 [Mem/Exec 2496MB - 1 Core/Exec] - 1000 Patients and 2000 Iterations - MonteCarlo Method"

spark-submit --class org.dataalgorithms.genomics.snp.scorev0.SparkScore --num-executors 47 --executor-memory 2496MB --executor-cores 1 /dev/shm/snp-score-1.0.0.jar /dev/shm/GenotypeMatrix.txt /dev/shm/TimeEvent.txt cox /dev/shm/SNPSets.txt /dev/shm/SNPWeight.txt 2000,1000 MonteCarlo

### Acknowledgment ###
The costs for using the Amazon Web Services (AWS) to conduct the experiments are covered by AWS in Education Research grant awards.

### Who do I talk to? ###
* Amir Bahmani, Alexander B. Sibley, 
Mahmoud Parsian, Kouros Owzar, Frank Mueller

