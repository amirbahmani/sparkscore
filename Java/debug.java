package org.dataalgorithms.SNP.spark;

import java.util.*;
import java.io.*;


// STEP-0: import required Java/Spark classes.
import org.dataalgorithms.util.SparkUtil;
import scala.Tuple2;
import scala.Tuple3;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.*;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class SparkSNP {
	public static void main(String[] args) throws Exception {

		
		// STEP-1: read input parameters and validate them
		/*
		 * Read input files 1) genotype matrix PID SNP_IDs PID1 SNP1 SNP2 SNP3
		 * ... PID1 0.21 -0.121 0.2121 Paired RDD -> <PID, SNPS>
		 * 
		 * 2) Time PID Time PID1 13.2121 Paired RDD -> <PID, Time>
		 * 
		 * 3) Event Indicator PID Event PID1 1 PID2 0 Paired RDD -> <PID, Event>
		 * 
		 * 4) Gene Subsets GSubID SNPIDs $XYZ9 [1] "rs688" "rs469" "rs557"
		 * "rs990" "rs346" "rs962" "rs501" "rs630" "rs913" "rs32" "rs802" <GSub
		 * ID>, List of SNPs
		 * 
		 * Create SubRDDs of G Matrix -> <GSubId, SubRDD>
		 * 
		 * 5) Weights SetID Weight XYZ9 0.5 HashMap <GSubID, Weight>
		 * 
		 */

		if (args.length < 5) {
			System.err.println(
					"Usage: SparkSNP <Gfilename> <TimeFilename> <EventFilename> <GSubsetFilename> <SubsetWeightFilename>");
			System.exit(1);
		}

		String inputPathG = args[0];
		System.out.println("Gene Matrix inputPath: <file>=" + inputPathG);

		String inputPathTimeEvent = args[1];
		System.out.println("Time inputPath: <file>=" + inputPathTimeEvent);

		//String inputPathEvent = args[2];
		//System.out.println("Event inputPath: <file>=" + inputPathEvent);

		String inputPathGSubset = args[2];
		System.out.println("Gene Subset inputPath: <file>=" + inputPathGSubset);

		//Per SNP
		String inputPathWeight = args[3];
		System.out.println("Weight inputPath: <file>=" + inputPathWeight);

		// STEP-2: Connect to the Sark master by creating JavaSparkContext
		// object
		final JavaSparkContext ctx = SparkUtil.createJavaSparkContext();
		final Broadcast<Map<Integer, Pair<Double, Integer>>> map =ctx.broadcast(initTimeEvent(inputPathTimeEvent));
		// STEP-3: Use ctx to create JavaRDD<String>
		// input record format: <PID><,><SNP value>
		JavaRDD<String> GMlines = ctx.textFile(inputPathG, 1);
		
		//<SNP_ID, <PID, val>>
		JavaPairRDD<Long, Tuple2<Integer, Double>> GMPairs = GMlines.mapToPair(new PairFunction<String, Long, Tuple2<Integer, Double>>() {
			public Tuple2<Long, Tuple2<Integer, Double>> call(String s) {
				String[] tokens = s.split(","); // PID,24
				long SNPID = Long.parseLong(tokens[0]);
				int PID = Integer.parseInt(tokens[1]);
				double GMval = Double.parseDouble(tokens[2]);
				Tuple2 value = new Tuple2<Integer, Double> (PID, GMval);
				return new Tuple2<Long, Tuple2<Integer, Double>>( SNPID, value);
			}
		});
		
		System.out.println("=========GMPairs DEBUG=============");
		List<Tuple2<Long, Tuple2<Integer, Double>>> debug5 = GMPairs.collect();
		for (Tuple2<Long, Tuple2<Integer, Double>> t2 : debug5) {
			//Long SNP_ID = t2._1;
			Tuple2<Integer, Double> GMRec = (Tuple2<Integer, Double>) t2._2;
			System.out.println("debug3 key=" + t2._1 + " PID: " + GMRec._1
					+ " Val: " + GMRec._2);
		}
		
		System.out.println("=========GMPairs GroupByKey DEBUG=============");
		List<Tuple2<Long, Iterable<Tuple2<Integer, Double>>>> debug6 = GMPairs.groupByKey().collect();
		for (Tuple2<Long, Iterable<Tuple2<Integer, Double>>> t2 : debug6) {
			//Long SNP_ID = t2._1;
			for (Tuple2<Integer, Double> val : t2._2){
				System.out.println(" PID: " + val._1
								+ " Val: " + val._2);
			}

			System.out.println("debug3 key=" + t2._1);
			//Tuple2<Integer, Double> GMRec = (Tuple2<Integer, Double>) t2._2;
			//System.out.println("debug3 key=" + t2._1 + " PID: " + GMRec._1
			//		+ " Val: " + GMRec._2);
		}
//List<Tuple2<Long, List<Tuple2<Integer, Double>>>> debug7
/*
		System.out.println("=========GMPairs ReduceByKey DEBUG=============");
		JavaPairRDD<Long, Tuple2<Integer,Double>> grouped= GMPairs.reduceByKey(
						  new Function2<Tuple2<Integer, Double>, Tuple2<Integer, Double>, Tuple2<Integer, Double>>() {
						    public Tuple2<Integer, Double> call(Tuple2<Integer, Double> a, Tuple2<Integer, Double> b) { 
						    	return new Tuple2(a._1()+b._1(), a._2()+b._2() ); }
						});

		System.out.println("=========ReduceByKey DEBUG=============");
		List<Tuple2<Long, Tuple2<Integer, Double>>> debug8 = grouped.collect();
		for (Tuple2<Long, Tuple2<Integer, Double>> t2 : debug8) {
			//Long SNP_ID = t2._1;
			Tuple2<Integer, Double> GMRec = (Tuple2<Integer, Double>) t2._2;
			System.out.println("debug3 key=" + t2._1 + " PID: " + GMRec._1
					+ " Val: " + GMRec._2);
		}
*/

		/* input record format: <PID><,><Time value>
		JavaRDD<String> TimeLines = ctx.textFile(inputPathTime, 1);
		JavaPairRDD<String, Double> timePairs = TimeLines.mapToPair(new PairFunction<String, String, Double>() {
			public Tuple2<String, Double> call(String s) {
				String[] tokens = s.split(","); // PID,24
				// System.out.println(tokens[0] + "," + tokens[1] + "," +
				// tokens[2]);
				double timevalue = Double.parseDouble(tokens[1]);
				return new Tuple2<String, Double>(tokens[0], timevalue);
			}
		});*/

		// input record format: <TID><,><Weight value>
		JavaRDD<String> WeightLines = ctx.textFile(inputPathWeight, 1);
		JavaPairRDD<String, Double> weightPairs = WeightLines.mapToPair(new PairFunction<String, String, Double>() {
			public Tuple2<String, Double> call(String s) {
				String[] tokens = s.split(","); // TID,24
				// System.out.println(tokens[0] + "," + tokens[1] + "," +
				// tokens[2]);
				double weight = Double.parseDouble(tokens[1]);
				return new Tuple2<String, Double>(tokens[0], weight);
			}
		});

		// input record format: <PID><,><Event value>
		/*JavaRDD<String> EventLines = ctx.textFile(inputPathEvent, 1);
		JavaPairRDD<String, Integer> eventPairs = EventLines.mapToPair(new PairFunction<String, String, Integer>() {
			public Tuple2<String, Integer> call(String s) {
				String[] tokens = s.split(","); // PID,24
				// System.out.println(tokens[0] + "," + tokens[1] + "," +
				// tokens[2]);
				int event = Integer.parseInt(tokens[1]);
				return new Tuple2<String, Integer>(tokens[0], event);
			}
		});*/
		
		
		
		// ONLY for DEBUGGING : validate INPUT, we collect all values from
		// JavaPairRDD<> and print it.
		/*List<Tuple2<String, Integer>> output = eventPairs.collect();
		for (Tuple2 t : output) {
			Integer eventvalue = (Integer) t._2;
			System.out.println(t._1 + "," + eventvalue);
		}

		List<Tuple2<String, Double>> output2 = timePairs.collect();
		for (Tuple2 t : output2) {
			Double timevalue = (Double) t._2;
			System.out.println(t._1 + "," + timevalue);
		}*/

		List<Tuple2<String, Double>> output3 = weightPairs.collect();
		for (Tuple2 t : output3) {
			Double weightvalue = (Double) t._2;
			System.out.println(t._1 + "," + weightvalue);
		}

		// STEP-4: Read Gene Subsets
		// JavaRDD<String> GSubListlines = ctx.textFile(inputPathGSubset, 1);
		Map<String, List<Long>> GSubMap = readGeneSubsets(inputPathGSubset);

		
		// Use Spark Sort later
		List<Long> fullSNPList = listSNPs(GSubMap);
		System.out.println(Arrays.toString(fullSNPList.toArray()));

		
/*

		System.out.println(Arrays.toString(fullSNPList.toArray()));

		for (Map.Entry<String, List<Integer>> ee : GSubMap.entrySet()) {
			String key = ee.getKey();
			List<Integer> values = ee.getValue();
			System.out.println("Key: " + key + " Values: " + values.size());
		}

		// STEP-5: Create sub-RDDs based on Map
		List<JavaPairRDD<String, List<Double>>> subPairedRDDList = new ArrayList<JavaPairRDD<String, List<Double>>>();
		for (Map.Entry<String, List<Integer>> entry : GSubMap.entrySet()) {
			// System.out.println(entry.getKey() + "/" + entry.getValue());
			JavaPairRDD<String, List<Double>> tempRDD = createPairRDD(GMlines, entry.getValue());
			//
			// List<Tuple2<String, List<Double>>> debug3 = tempRDD.collect();
			// for (Tuple2<String, Tuple2<List<Double>, Integer>> t2 : debug3) {
			// System.out.println("debug3 key="+t2._1+"\t value="+t2._2); }
			//

			JavaPairRDD<String, Tuple2<List<Double>, Integer>> tempRDD2 = tempRDD.join(eventPairs);

			//
			 // System.out.println("=========DEBUG=============");
			// List<Tuple2<String, Tuple2<List<Double>, Integer>>> debug4 =
			// tempRDD2.collect(); for (Tuple2<String, Tuple2<List<Double>,
			// Integer>> t2 : debug4) { Tuple2<List<Double>, Integer> temp_t =
			// (Tuple2<List<Double>, Integer>) t2._2; System.out.println(
			// "debug3 key="+t2._1 + " double list size: " + temp_t._1.size() +
			// "\t event value 2= "+ temp_t._2); }
			//

			JavaPairRDD<String, Tuple2<Tuple2<List<Double>, Integer>, Double>> tempRDD3 = tempRDD2.join(timePairs);

			System.out.println("=========DEBUG=============");
			List<Tuple2<String, Tuple2<Tuple2<List<Double>, Integer>, Double>>> debug5 = tempRDD3.collect();
			for (Tuple2<String, Tuple2<Tuple2<List<Double>, Integer>, Double>> t2 : debug5) {
				Tuple2<Tuple2<List<Double>, Integer>, Double> temp_time = (Tuple2<Tuple2<List<Double>, Integer>, Double>) t2._2;
				Tuple2<List<Double>, Integer> temp_t = (Tuple2<List<Double>, Integer>) temp_time._1;
				System.out.println("debug3 key=" + t2._1 + " double list size: " + temp_t._1.size()
						+ "\t event value 2= " + temp_t._2 + " Time: " + temp_time._2);
			}

			 //
			 // tempRDD3.groupByKey();
			 // System.out.println("=========DEBUG=============");
			 // List<Tuple2<String, Tuple2<Tuple2<List<Double>, Integer>,
			 // Double>>> debug6 = tempRDD3.collect(); for (Tuple2<String,
			 // Tuple2<Tuple2<List<Double>, Integer>, Double>> t2 : debug6) {
			 // Tuple2<Tuple2<List<Double>, Integer>, Double> temp_time =
			 // (Tuple2<Tuple2<List<Double>, Integer>, Double>) t2._2;
			 // Tuple2<List<Double>, Integer> temp_t = (Tuple2<List<Double>,
			 // Integer>) temp_time._1; System.out.println("debug3 key="+t2._1 +
			 // " double list size: " + temp_t._1.size() +"\t event value 2= "+
			 // temp_t._2 + " Time: " + temp_time._2); }
			 //
		}

		// STEP-6: call Plugin Function
		// JavaPairRDD<String, Double> U_i_tPairedRDD;;
		// List<JavaPairRDD<String, Double>> U_i_tPairedRDDList = new
		// ArrayList<JavaPairRDD<String, Double>>();
		// int index=0;
		// for (Map.Entry<String, List<Integer>> entry : GSubMap.entrySet())
		{
			// U_i_tPairedRDD = (entry.getKey(),
			// subPairedRDDList.get(index).map(new PlugIn())); // .reduce(new
			// Sum()));
			// index++;
		}
		// for (int i = 0; i < subPairedRDDList.size(); i++) {
		// U_i_tPairedRDD = (TID, subPairedRDDList.get(i).map(new
		// PlugIn()).reduce(new Sum()));
		// maptopair
		// }

		// STEP-7: Multiply U[all<i>,t] by W_t and Sum all the values and return
		// S(i)
		// U_i_tPairedRDD.union(weightRDD);
		// U_i_tPairedRDD.groupByKey();
		// U_i_tPairedRDD.reduce(new Sum());
*/
		ctx.stop();
		System.exit(0);
	}

	static JavaPairRDD<String, List<Double>> createPairRDD(JavaRDD lines, final List<Integer> indexes) {

		JavaPairRDD<String, List<Double>> pairs = lines.mapToPair(new PairFunction<String, String, List<Double>>() {
			public Tuple2<String, List<Double>> call(String s) {
				String[] tokens = s.split(","); // PID,0.2 15.2 -3.76
				System.out.println(tokens[0] + "," + tokens[1]);
				List<Double> doubleList = new ArrayList<Double>();

				String[] stringDoubleList;
				stringDoubleList = tokens[1].split(" ");
				System.out.println("<========= " + stringDoubleList.length + "=========>");
				int index = 0;
				int indexGSub = 0;
				for (Integer item : indexes) {
					// while(index != indexes.get(indexGSub)){
					// index++;
					// }
					System.out.println("Index -> " + item);
					doubleList.add(Double.parseDouble(stringDoubleList[item]));
					// indexGSub++;
					// if (indexes.size()>1)
					// indexes.remove(0);
				}
				System.out.println("<========= " + stringDoubleList.length + "=========>");

				// PID , <0.2 15.2 -3.76>
				return new Tuple2<String, List<Double>>(tokens[0], doubleList);
			}
		});

		return pairs;

	}

	/*
	 * static List<Tuple2<Integer,Integer>>
	 * iterableToList(Iterable<Tuple2<Integer,Integer>> iterable) {
	 * List<Tuple2<Integer,Integer>> list = new
	 * ArrayList<Tuple2<Integer,Integer>>(); for (Tuple2<Integer,Integer> item :
	 * iterable) { list.add(item); } return list; }
	 */

	static List<Long> listSNPs(Map<String, List<Long>> temp) {

		List<Long> longFullSNPList = new ArrayList<Long>();
		Set<Long> listSet = new HashSet<Long>();
		for (Map.Entry<String, List<Long>> ee : temp.entrySet()) {
			listSet.addAll(ee.getValue());
			// if (intFullSNPList.isEmpty()){
			// intFullSNPList.addAll(ee.getValue());
			// }
			// else{
			// intFullSNPList.retainAll(ee.getValue());
			// }
			// ListUtils.intersection(intFullSNPList, ee.getValue());

		}
		longFullSNPList.addAll(listSet);

		Collections.sort(longFullSNPList);

		return longFullSNPList;
	}

	static Map<String, List<Long>> readGeneSubsets(String filename) {
		Map<String, List<Long>> myMap = new HashMap<String, List<Long>>();

		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(filename));
			String line = reader.readLine();
			while (line != null) {
				try {
					String[] tokens = line.split(","); // x,2,5
					List<Long> intList = new ArrayList<Long>();

					String[] stringIntegerList;
					stringIntegerList = tokens[1].split(" ");

					for (String item : stringIntegerList) {
						intList.add(Long.parseLong(item));
					}

					myMap.put(tokens[0], intList);

				} catch (NumberFormatException ex) {
					System.out.println(ex.getMessage());
				}

				line = reader.readLine();
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		return myMap;
	}

	static Map<Integer, Pair<Double, Integer>> initTimeEvent(String Filename) throws FileNotFoundException {
		    Scanner scanTimeEvent = new Scanner(new File(Filename));
		    Map<Integer, Pair<Double, Integer>> map = new HashMap<Integer, Pair<Double, Integer>>();
		   while (scanTimeEvent.hasNextLine()) {
			   String s = scanTimeEvent.nextLine();
			   String[] tokens = s.split(",");
			   int PID = Integer.parseInt(tokens[0]);
			   double Time = Double.parseDouble(tokens[1]);
			   int Event =  Integer.parseInt(tokens[2]);
			   Pair<Double, Integer> TimeEventPair = new Pair<Double, Integer>(Time, Event);
			   map.put(PID, TimeEventPair);
		    }
		    return map;//callSignList.toArray(new String[0]);
		  }
}


class PlugIn implements Function<String, Integer> {
	public Integer call(String s) {
		return s.length();
	}
}

class Pair<Double, Integer> {

	  private final Double survivalTime;
	  private final Integer eventIndicator;

	  public Pair(Double survivalTime, Integer eventIndicator) {
	    this.survivalTime = survivalTime;
	    this.eventIndicator = eventIndicator;
	  }

	  public Double getSurvivalTime() { return survivalTime; }
	  public Integer getEventIndicator() { return eventIndicator; }

	  @Override
	  public int hashCode() { return survivalTime.hashCode() ^ eventIndicator.hashCode(); }

	  @Override
	  public boolean equals(Object o) {
	    if (!(o instanceof Pair)) return false;
	    Pair pairo = (Pair) o;
	    return this.survivalTime.equals(pairo.getSurvivalTime()) &&
	           this.eventIndicator.equals(pairo.getEventIndicator());
	  }

	}

