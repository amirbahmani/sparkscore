package org.dataalgorithms.genomics.snp.score;

/**
 * This is driver class for running cox, binomial, and gaussian algorithms.
 *
 */
public class SparkScore {

	public static int numPermutation = 0;
	public static int numPatients = 0;
	public static boolean DEBUG = false;
	public static boolean CACHE = false;

	public static void main(String[] args) throws Exception {

		/*
		 * Assumption: Genotype Matrix created based on Gene Subsets in
		 * advance!!! Read input files
		 *
		 * 1) Genotype matrix (Column-based): 
		 *    KEY:SNP ID and VALUE: PID1,Val1,PID2,Val2 ... PIDn,Valn 
		 *    Format: SNPID&PID1,Val1 PID2,Val2 ..., PIDn,Valn
		 * 
		 * 2) Pairs of Time and Event per patient; 
		 *    PID1,Time1,Event1, ..., PIDn,Timen,Eventn 
		 *    HashMap for Cox: <PID, <Time,Event>> for Gaussian
		 *    and Binomial: PID,EVENT {int, double}
		 *
		 *
		 * 3) Type score=c("cox", "binomial", "gaussian")
		 *
		 *
		 * 4) Gene Subsets GSubID SNPIDs $XYZ9 [1] "rs688" "rs469" "rs557"
		 * "rs990" "rs346" "rs962" "rs501" "rs630" "rs913" "rs32" "rs802" <GSub
		 * ID>, List of SNPs
		 *
		 * 5) Weights of SNPs - HashMap <SNPID, Weight>
		 *
		 * 6) [Optional] Number of Patients, Number of Iterations
		 * 
		 * 7) Type of Permutations: Scrambling or MonteCarlo
		 *
		 */

		// STEP-1: read input parameters and validate them
		if (args.length < 5) {
			System.err.println("Usage: SparkSNP <Gfilename> <TimeEventFilename> <FunctionType> <GSubsetFilename> <Num of Patients>,<Num of Permutations> <SNPWeightFilename> <CovFilename>");
			System.exit(1);
		}

		String inputPathG = args[0];
		System.out.println("Gene Matrix inputPath: <file>=" + inputPathG);

		String inputPathTimeEvent = args[1];
		System.out.println("Time-Event inputPath: <file>=" + inputPathTimeEvent);

		String inputFunctionType = args[2];
		System.out.println("Input Function Type: cox, binomial , gaussian" + inputFunctionType);

		String inputPathGSubset = args[3];
		System.out.println("Gene Subset inputPath: <file>=" + inputPathGSubset);

		String inputPathWeight = args[4];
		System.out.println("Weight inputPath: <file>=" + inputPathWeight);

		String inputTypePermutaion = "";
		String inputPathWG = "";

		if (args.length >= 6) {
			String[] tokens = args[5].split(",");
			numPermutation = Integer.parseInt(tokens[0]);
			numPatients = Integer.parseInt(tokens[1]);
			System.out.println("Number of Permutations: " + numPermutation);
			System.out.println("Number of Patients: " + numPatients);

			if (args.length >= 7) {
				inputTypePermutaion = args[6]; // Scrambling or MonteCarlo
				System.out.println("Permutation Type = " + inputTypePermutaion);
			}

			if (args.length >= 8) {
				inputPathWG = args[7];
				System.out.println("Patient Weight inputPath: <file>=" + inputPathWG);
			}
		}

		if (inputFunctionType.equalsIgnoreCase("cox")) {
			Cox.numPermutation = numPermutation;
			Cox.numPatients = numPatients;
			Cox.DEBUG = DEBUG;
			Cox.CACHE = CACHE;
			Cox.run(inputPathG, inputPathTimeEvent, inputPathGSubset, inputPathWeight, inputTypePermutaion,
					inputPathWG);
		}
		else if (inputFunctionType.equalsIgnoreCase("binomial")) {
			Binomial.numPermutation = numPermutation;
			Binomial.numPatients = numPatients;
			Binomial.DEBUG = DEBUG;
			Binomial.CACHE = CACHE;
			Binomial.run(inputPathG, inputPathTimeEvent, inputPathGSubset, inputPathWeight, inputTypePermutaion,
					inputPathWG);
		}
		else if (inputFunctionType.equalsIgnoreCase("gaussian")) {
			Gaussian.numPermutation = numPermutation;
			Gaussian.numPatients = numPatients;
			Gaussian.DEBUG = DEBUG;
			Gaussian.CACHE = CACHE;
			Gaussian.run(inputPathG, inputPathTimeEvent, inputPathGSubset, inputPathWeight, inputTypePermutaion,
					inputPathWG);
		}
		else {
			throw new Exception("function type is undefined: inputFunctionType="+inputFunctionType);
		}
	}
}
