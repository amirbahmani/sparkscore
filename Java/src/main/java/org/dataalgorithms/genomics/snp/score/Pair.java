/**
 * This package (org.dataalgorithms.genomics.snp.score) contains source code for 
 * score statistics.
 * This class represents a pair of "survival time" and "event indicator".
 *
 */


package org.dataalgorithms.genomics.snp.score;

import java.io.Serializable;

class Pair<Double, Integer> implements Serializable {

        private final Double survivalTime;
        private final Integer eventIndicator;

        public Pair(Double survivalTime, Integer eventIndicator) {
                this.survivalTime = survivalTime;
                this.eventIndicator = eventIndicator;
        }

        public Double getSurvivalTime() {
                return survivalTime;
        }

        public Integer getEventIndicator() {
                return eventIndicator;
        }

        @Override
        public int hashCode() {
                return survivalTime.hashCode() ^ eventIndicator.hashCode();
        }

        @Override
        public boolean equals(Object o) {
                if (!(o instanceof Pair)) {
                    return false;
                }
                Pair pairo = (Pair) o;
                return this.survivalTime.equals(pairo.getSurvivalTime())
                                && this.eventIndicator.equals(pairo.getEventIndicator());
        }

}


