/**
 * This package (org.dataalgorithms.genomics.snp.score) contains source code for 
 * Binomial score statistics.
 *
 */

package org.dataalgorithms.genomics.snp.score;

import java.util.*;
import java.io.*;

import java.io.Serializable;
import scala.Tuple2;
import scala.Tuple3;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.*;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;

import org.apache.spark.mllib.linalg.distributed.BlockMatrix;
import org.apache.spark.mllib.linalg.distributed.CoordinateMatrix;
import org.apache.spark.mllib.linalg.distributed.IndexedRowMatrix;
import org.apache.spark.mllib.linalg.distributed.MatrixEntry;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Binomial {

	public static int iter = 0;
	public static int numPermutation = 0;
	public static int numPatients = 0;
	public static boolean DEBUG= false;
	public static boolean CACHE=false;
	
	static void run(String inputPathG, String inputPathTimeEvent, String inputPathGSubset, String inputPathWeight,
			String inputTypePermutaion, String inputPathWG) throws Exception {
		//STEP1: U Matrix - Initialization
		JavaPairRDD<String, List<Tuple2<Integer, Double>>> Gij_GjMean = null;
		JavaPairRDD<String, List<Tuple2<Integer, Double>>> U0;
		
		// STEP-2: Connect to the Sark master by creating JavaSparkContext object
		final JavaSparkContext ctx = new JavaSparkContext();

		// STEP-3: Read Genotype Matrix : input record format: SID&PID1,Val1
		// PID2,Val2 ... PIDn,Valn
		JavaRDD<String> GMlines = ctx.textFile(inputPathG, 5);

		// STEP-4: Read Gene Sets
		final Map<String, List<String>> GSubMap = readGeneSubsets(inputPathGSubset);

		// STEP-5: Create a set of all SNPs in geneSets
		final Map<String, Boolean> AllSNPs = createMapAllSNPs(GSubMap);

		// STEP-6.1: Filter out SNPs not participating in the calculation
		// STEP-6.2: Map the SNP sets after filtering
		JavaPairRDD<String, String> GMPairs = GMlines
				
				  //Worst Case is to compute all of them 
				.filter(new
				  Function<String, Boolean>() { public Boolean call(String s) {
				  String[] tokens = s.split("&"); // SID&PID1,Val1 PID2,Val2
				  String SNPID = tokens[0]; String PIDsVals = tokens[1]; Object
				  obj = AllSNPs.get(SNPID); if(obj!=null) return true; else
				  return false; } })
				
				.mapToPair(new PairFunction<String, String, String>() {
					public Tuple2<String, String> call(String s) {
						String[] tokens = s.split("&"); // SID&PID1,Val1
														// PID2,Val2
						String SNPID = tokens[0];
						String PIDsVals = tokens[1];
						return new Tuple2<String, String>(SNPID, PIDsVals);

					}
				});

		//Cache U0 for MonteCarlo Execution
		if (numPermutation > 0) 
			if (inputTypePermutaion.equals("Scrambling") && CACHE) {
				System.out.println("Cached GMPairs matrix!");
				GMPairs.cache();
			}

		
		if(DEBUG){
			System.out.println("=========GMPairs Filtering Test DEBUG=============");
			List<Tuple2<String, String>> debug1 = GMPairs.collect();
			for (Tuple2<String, String> t2 : debug1) {
				System.out.println("debug3 SNP ID=" + t2._1 + " Val: " + t2._2);
			}
		}
		// STEP-7: Read SNP Weights
		JavaRDD<String> SNPWeightlines = ctx.textFile(inputPathWeight, 1);

		JavaPairRDD<String, Double> SNPWeightPairs = SNPWeightlines.filter(new Function<String, Boolean>() {
			public Boolean call(String s) {
				String[] tokens = s.split(" "); // SID Weight
				String SNPID = tokens[0];
				Object obj = AllSNPs.get(SNPID);
				if (obj != null)
					return true;
				else
					return false;
			}
		}).mapToPair(new PairFunction<String, String, Double>() {
			public Tuple2<String, Double> call(String s) {
				String[] tokens = s.split(" "); // SID Weight
				String SNPID = tokens[0];
				double weight = Double.parseDouble(tokens[1]);
				double squreW = weight*weight; 
				return new Tuple2<String, Double>(SNPID, squreW);
			}
		});
		
		if(DEBUG){
			System.out.println("=========SNP Weight Pairs Test DEBUG=============");
			List<Tuple2<String, Double>> debugW = SNPWeightPairs.collect();
			for (Tuple2<String, Double> t2 : debugW) {
				System.out.println("debug3 SNP ID=" + t2._1 + " Weight: " + t2._2);
			}
		}
		
		
		// STEP-8:Score Statistics
		// STEP-1: Retrive Gij from Genotype matrix, and calcualte "Gij-GjMean"
			Gij_GjMean = GMPairs
					.mapToPair(new PairFunction<Tuple2<String, String>, String, List<Tuple2<Integer, Double>>>() {
						public Tuple2<String, List<Tuple2<Integer, Double>>> call(Tuple2<String, String> s) {

							String SID = s._1; // SNP ID
							String[] tokens = s._2.split(" "); // PID1,Val1 PID2,Val2 ... PID_n,Val_n
							Map<Integer, Double> SNPMapVal = new HashMap<Integer, Double>();
							List<Tuple2<Integer, Double>> PID_Gij_GMean = new ArrayList<Tuple2<Integer, Double>>();
							
							//STEP-1-1 Create a map of <PID, Val> for the current SNP
							for (String tempS : tokens) {
								String[] tempPIDsVals = tempS.split(",");
								int PID = Integer.parseInt(tempPIDsVals[0]);
								double val = Double.parseDouble(tempPIDsVals[1]);
								SNPMapVal.put(PID, val);
							}

							//STEP-1-2: Calcaulte GjMean per each SNPj 
							double sum =0;
							for (Map.Entry<Integer, Double> mainEntry : SNPMapVal.entrySet()) {
								double Gij = mainEntry.getValue();
								sum = sum + Gij;
							 }
							double Gmean = sum / SNPMapVal.size();
							
							//STEP-1-3: Create a list of KEY:PID,VALUE:Gij-Gmean
							for (Map.Entry<Integer, Double> mainEntry : SNPMapVal.entrySet()) {
								int PID = mainEntry.getKey();
								double Gij = mainEntry.getValue();
								Tuple2<Integer, Double> PID_Gij_GMean_Elem = new Tuple2<Integer, Double>(PID, Gij - Gmean);
								PID_Gij_GMean.add(PID_Gij_GMean_Elem);
							}
							//STEP-1-4: Return back a list of SNPj, List of <PIDi, Gij-GjMean>
							return new Tuple2<String, List<Tuple2<Integer, Double>>>(SID, PID_Gij_GMean);

						 }
			});

			 JavaPairRDD<Integer, Double> eventPairsYi_YMean;
			 System.out.println("=========Binomial=============");
				 JavaRDD<String> eventRDD = ctx.textFile(inputPathTimeEvent, 1);
				 JavaPairRDD<Integer, Integer> eventPairs =
						eventRDD.mapToPair(
						 new PairFunction<String, Integer, Integer>() {
							 public Tuple2<Integer, Integer> call(String s) {
								 String[] tokens = s.split(","); // PID,24
								 int PID = Integer.parseInt(tokens[0]);
								 int Event = Integer.parseInt(tokens[1]);
								 return new Tuple2<Integer, Integer>(PID, Event);
							 }
						});
				 //STEP-2: Calculate mean value of Event vector and broadcast it
				 final Broadcast<Double> YMean = ctx.broadcast(eventPairs.mapToDouble(tuple -> tuple._2).mean());
				 //STEP-3: Calculate Yi-Ymean and broadcast it
				 eventPairsYi_YMean = eventPairs
						 .mapToPair(new PairFunction<Tuple2<Integer, Integer>, Integer, Double>() {
							 public Tuple2<Integer, Double> call(Tuple2<Integer, Integer> s) {
								 return new Tuple2<Integer, Double>(s._1, (double)(s._2 - YMean.value()));
							 }	
						 });
			//STEP-4: Broadcast Event Indicators	
			final Broadcast<Map<Integer, Double>>  map = ctx.broadcast(eventPairsYi_YMean.collectAsMap());
				 
			//STEP-5: U0
			U0 = Gij_GjMean
					.mapToPair(new PairFunction<Tuple2<String, List<Tuple2<Integer, Double>>>, String, List<Tuple2<Integer, Double>>>() {
						public Tuple2<String, List<Tuple2<Integer, Double>>> call(Tuple2<String, List<Tuple2<Integer, Double>>> s) {

							String SID = s._1; // SNP ID
							List<Tuple2<Integer, Double>> PID_Gij_GMean_Times_Yi_YMean = new ArrayList<Tuple2<Integer, Double>>();
							
							//STEP-1-1 Create a map of <PID, Val> for the current SNP
							for (Tuple2<Integer, Double> tempS : s._2) {
								int PID = tempS._1;
								double Uij = tempS._2 * map.value().get(PID);
								Tuple2<Integer, Double> PID_Gij_GMean_Times_Yi_YMean_Elem = new Tuple2<Integer, Double>(PID, Uij);
								PID_Gij_GMean_Times_Yi_YMean.add(PID_Gij_GMean_Times_Yi_YMean_Elem);
							}
									
							//STEP-1-2: Return back a list of SNPj, List of <PIDi, Gij-GjMean>
							return new Tuple2<String, List<Tuple2<Integer, Double>>>(SID, PID_Gij_GMean_Times_Yi_YMean);
							
						}
					});

				
		if(DEBUG){
			System.out.println("========= U Matrix  Test DEBUG=============");
			List<Tuple2<String, List<Tuple2<Integer, Double>>>> debug2 = U0.collect();
			for (Tuple2<String, List<Tuple2<Integer, Double>>> t2 : debug2) {
				List<Tuple2<Integer, Double>> X = t2._2;
				for (Tuple2<Integer, Double> m : X)
					System.out.println("debug3 SNP ID=" + t2._1 + " PID: " + m._1 + " Val: " + m._2);
				System.out.println("debug3 SNP ID=" + t2._1 + " Size Val: " + t2._2.size());
			}
		}

		//Cache U0 for MonteCarlo Execution
		if (numPermutation > 0) 
			if (inputTypePermutaion.equals("MonteCarlo") && CACHE) {
				System.out.println("Cached U matrix!");
				U0.cache();
			}
		
		
		// STEP-6: Calculate Sigma of U[i, j] per each SNP <SNP_ID,
		// cox_value>
		JavaPairRDD<String, Double> sumU0 = U0
				.mapToPair(new PairFunction<Tuple2<String, List<Tuple2<Integer, Double>>>, String, Double>() {
					public Tuple2<String, Double> call(Tuple2<String, List<Tuple2<Integer, Double>>> s) {

						String SID = s._1;
						List<Tuple2<Integer, Double>> PID_VAl = s._2;

						double val_U = 0;
						for (Tuple2<Integer, Double> SNP_VAL : PID_VAl) {
							val_U = val_U + SNP_VAL._2;
						}
						return new Tuple2<String, Double>(SID, val_U);
					}
				});

		
		
		// STEP 6: Multiply SNP by their Weights
		JavaPairRDD<String, Tuple2<Double, Double>> joinOuterSigma = sumU0.join(SNPWeightPairs); //squareU0

		JavaPairRDD<String, Double> outerSigma = joinOuterSigma
				.mapToPair(new PairFunction<Tuple2<String, Tuple2<Double, Double>>, String, Double>() {
					public Tuple2<String, Double> call(Tuple2<String, Tuple2<Double, Double>> s) {

						return new Tuple2<String, Double>(s._1, (s._2._1*s._2._1) * s._2._2);
					}
				});
		
		if(DEBUG){
			System.out.println("========= Outer Sigma => Binomial per SNP  Test DEBUG=============");
			List<Tuple2<String, Double>> debug3 = outerSigma.collect();
			for (Tuple2<String, Double> t2 : debug3) {
				System.out.println("debug3 SNP ID=" + t2._1 + " SNP Score: " + t2._2);
			}
		}

		// STEP-6-1: Calculate S0 per each GeneSet
		Map<String, Double> SUM_U_Map = outerSigma.collectAsMap();
		// STEP-6-2:
		final Map<String, Double> Score0Map = calculateScoreStatistics(GSubMap, SUM_U_Map);

		if(DEBUG){
			System.out.println("========= S0 Score-   DEBUG=============");
			for (Map.Entry<String, Double> entryS0 : Score0Map.entrySet()) {
				System.out.println("SNPSet ID=" + entryS0.getKey() + " S0 Score: " + entryS0.getValue());
			}
		}



		// <--------------------------------------PERMUTATION------------------------------------------------>//

		if (numPermutation > 0) {
			
			System.out.println("========= Permutation Sampling =============");
			JavaPairRDD<String, List<Tuple2<Integer, Double>>> U_Si;
			
			if (inputTypePermutaion.equals("Scrambling")) {
				// Solution1: Scrambling
				System.out.println("========= Scrambling =============");

				Map<String, Long> incrementCounters = new HashMap<String, Long>();
				
				//STEP-1: Scambling Initialization 
				final Broadcast<Map<Integer, Map<Integer,Double>>> ScramblingSampling = ctx
						.broadcast(initScaramblingMatrix(numPermutation, numPatients,  map.value() )); 					
				while (iter < numPermutation) {
						
					//STEP-2: U_i
					U_Si = Gij_GjMean
							.mapToPair(new PairFunction<Tuple2<String, List<Tuple2<Integer, Double>>>, String, List<Tuple2<Integer, Double>>>() {
								public Tuple2<String, List<Tuple2<Integer, Double>>> call(Tuple2<String, List<Tuple2<Integer, Double>>> s) {
									
										String SID = s._1; // SNP ID
										List<Tuple2<Integer, Double>> PID_Gij_GMean_Times_Yi_YMean = new ArrayList<Tuple2<Integer, Double>>();

										//Retrive the corresponding scrambled TimeEvent values	
										Map<Integer, Double> iterMapTimeEvent = ScramblingSampling.value().get(iter);
									
										
										//STEP-1-1 Create a map of <PID, Val> for the current SNP
										for (Tuple2<Integer, Double> tempS : s._2) {
											int PID = tempS._1;
											double Uij = tempS._2 * iterMapTimeEvent.get(PID);
											Tuple2<Integer, Double> PID_Gij_GMean_Times_Yi_YMean_Elem = new Tuple2<Integer, Double>(PID, Uij);
											PID_Gij_GMean_Times_Yi_YMean.add(PID_Gij_GMean_Times_Yi_YMean_Elem);
										}
												
										//STEP-1-2: Return back a list of SNPj, List of <PIDi, Gij-GjMean>
										return new Tuple2<String, List<Tuple2<Integer, Double>>>(SID, PID_Gij_GMean_Times_Yi_YMean);
										
									}
								});						
					
					
					if(DEBUG){
						System.out.println("========= U Matrix  Test DEBUG=============");
						List<Tuple2<String, List<Tuple2<Integer, Double>>>> debug2i = U_Si.collect();
						for (Tuple2<String, List<Tuple2<Integer, Double>>> t2 : debug2i) {
							List<Tuple2<Integer, Double>> X = t2._2;
							for (Tuple2<Integer, Double> m : X)
								System.out.println("debug3 SNP ID=" + t2._1 + " PID: " + m._1 + " Val: " + m._2);
							System.out.println("debug3 SNP ID=" + t2._1 + " Size Val: " + t2._2.size());
						}
					}
					// STEP-3: Calculate Sigma of U[i, j] per each SNP <SNP_ID,
					// cox_value>
					JavaPairRDD<String, Double> sumU_Si = U_Si
							.mapToPair(new PairFunction<Tuple2<String, List<Tuple2<Integer, Double>>>, String, Double>() {
								public Tuple2<String, Double> call(Tuple2<String, List<Tuple2<Integer, Double>>> s) {

									String SID = s._1;
									List<Tuple2<Integer, Double>> PID_U_VAl = s._2;

									double val_U = 0;
									for (Tuple2<Integer, Double> U_VAL : PID_U_VAl) {
										double U_val2 = U_VAL._2;
										val_U = val_U + U_val2;
									}
									return new Tuple2<String, Double>(SID, val_U);
								}
							});

					
					// STEP 3: Multiply SNP by their Weights
					JavaPairRDD<String, Tuple2<Double, Double>> joinOuterSigma_Si = sumU_Si // squareU_Si
							.join(SNPWeightPairs);

					JavaPairRDD<String, Double> outerSigma_Si = joinOuterSigma_Si
							.mapToPair(new PairFunction<Tuple2<String, Tuple2<Double, Double>>, String, Double>() {
								public Tuple2<String, Double> call(Tuple2<String, Tuple2<Double, Double>> s) {

									return new Tuple2<String, Double>(s._1, (s._2._1*s._2._1) * s._2._2);
								}
							});

					Map<String, Double> SUM_U_Map_Si = outerSigma_Si.collectAsMap();
					Map<String, Double> ScoreSiMap = calculateScoreStatistics(GSubMap, SUM_U_Map_Si);

					for (Map.Entry<String, Double> entryInc : ScoreSiMap.entrySet()) {

						if (entryInc.getValue() >= Score0Map.get(entryInc.getKey())) {
							Long count = incrementCounters.get(entryInc.getKey());
							if (count == null) {
								incrementCounters.put(entryInc.getKey(), (long) 1);
							} else {
								incrementCounters.put(entryInc.getKey(), count + 1);	
							}
						}
					}	
						iter++;
					}

		if(DEBUG){
			for (Map.Entry<String, Long> entryInc : incrementCounters.entrySet()) {
				System.out.println("SNPSet ID=" + entryInc.getKey() + " Inc Value: " + entryInc.getValue());
			}
		}
				
		} else if (inputTypePermutaion.equals("MonteCarlo")) {
				// Solution2: MonteCarlo
				System.out.println("========= MonteCarlo =============");

				// Due to space we need for U[i, t] matrix per each
				// iteration,
				// we should implement it each parallel
				// Solution1: final Solution2: Broadcast
				final Broadcast<Map<Integer, ArrayList<Double>>> MonteCarloPermutations = ctx
						.broadcast(initMonteCarloMatrix(numPermutation, numPatients)); //
				
				if(DEBUG){
					for (final Map.Entry<Integer, ArrayList<Double>> entryPerm : MonteCarloPermutations.value()
							.entrySet()) {
						System.out.println(
								"Permutation ID=" + entryPerm.getKey() + " Val Size: " + entryPerm.getValue().size());

						ArrayList<Double> X = entryPerm.getValue();
						for (Double tmp : X)
							System.out.println("Permutation ID=" + entryPerm.getKey() + " Val: " + tmp);
					}
				}
				
				Map<String, Long> incrementCounters = new HashMap<String, Long>();

				while (iter < numPermutation) {

					// STEP-1: Calculate (weight*U[i, j]) per each SNP
					// <SNP_ID,
					// cox_value>
					JavaPairRDD<String, Double> sumU_Si = U0.mapToPair(
							new PairFunction<Tuple2<String, List<Tuple2<Integer, Double>>>, String, Double>() {
								public Tuple2<String, Double> call(
										Tuple2<String, List<Tuple2<Integer, Double>>> s) {
									final ArrayList<Double> currentPermutationWeights = new ArrayList(
											MonteCarloPermutations.value().get(iter));

									String SID = s._1;
									List<Tuple2<Integer, Double>> PID_U_VAl = s._2;
									double val_U = 0;
									for (Tuple2<Integer, Double> U_VAL : PID_U_VAl) {
										int PID = U_VAL._1; // PID starts from 1 to n
										double U_val2 = U_VAL._2;
										U_val2 = currentPermutationWeights.get(PID - 1) * U_val2;
										val_U = val_U + U_val2;
									}
									return new Tuple2<String, Double>(SID, val_U);
								}
							});

					// STEP 3: Multiply SNP by their Weights
					JavaPairRDD<String, Tuple2<Double, Double>> joinOuterSigma_Si = sumU_Si // squareU0_Si
							.join(SNPWeightPairs);

					JavaPairRDD<String, Double> outerSigma_Si = joinOuterSigma_Si
							.mapToPair(new PairFunction<Tuple2<String, Tuple2<Double, Double>>, String, Double>() {
								public Tuple2<String, Double> call(Tuple2<String, Tuple2<Double, Double>> s) {

									return new Tuple2<String, Double>(s._1, (s._2._1 * s._2._1) * s._2._2);
								}
							});

					Map<String, Double> SUM_U_Map_Si = outerSigma_Si.collectAsMap();
					Map<String, Double> ScoreSiMap = calculateScoreStatistics(GSubMap, SUM_U_Map_Si);

					for (Map.Entry<String, Double> entryInc : ScoreSiMap.entrySet()) {

						if (entryInc.getValue() >= Score0Map.get(entryInc.getKey())) {
							Long count = incrementCounters.get(entryInc.getKey());
							if (count == null) {
								incrementCounters.put(entryInc.getKey(), (long) 1);
							} else {
								incrementCounters.put(entryInc.getKey(), count + 1);
							}
						}
					}
					iter++;
				}
				if(DEBUG){	
					for (Map.Entry<String, Long> entryInc : incrementCounters.entrySet()) {
						System.out.println("SNPSet ID=" + entryInc.getKey() + " Inc Value: " + entryInc.getValue());
					}
				}

			} // end of MonteCarlo
		} // end of Permutations

		System.out.println("=========Final=============");


		ctx.stop();
		System.exit(0);
	}

	double calcMean(Map<Integer, Integer> map) {

		long Sum = 0;
		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			Sum = Sum + entry.getValue();
		}
		double mean = Sum / map.size();

		return mean;

	}

	static JavaPairRDD<String, List<Double>> createPairRDD(JavaRDD lines, final List<Integer> indexes) {
		JavaPairRDD<String, List<Double>> pairs = lines.mapToPair(new PairFunction<String, String, List<Double>>() {
			public Tuple2<String, List<Double>> call(String s) {
				String[] tokens = s.split(","); // PID,0.2 15.2 -3.76
				List<Double> doubleList = new ArrayList<Double>();
				String[] stringDoubleList;
				stringDoubleList = tokens[1].split(" ");
				
				if(DEBUG){
					System.out.println(tokens[0] + "," + tokens[1]);
					System.out.println("<========= " + stringDoubleList.length + "=========>");
				}

				int index = 0;
				int indexGSub = 0;
				for (Integer item : indexes) {
					// while(index != indexes.get(indexGSub)){
					// index++;
					// }
					if(DEBUG){
						System.out.println("Index -> " + item);
					}
					doubleList.add(Double.parseDouble(stringDoubleList[item]));
					// indexGSub++;
					// if (indexes.size()>1)
					// indexes.remove(0);
				}

				// PID , <0.2 15.2 -3.76>
				return new Tuple2<String, List<Double>>(tokens[0], doubleList);
			}
		});

		return pairs;

	}

	/*
	 * static List<Tuple2<Integer,Integer>>
	 * iterableToList(Iterable<Tuple2<Integer,Integer>> iterable) {
	 * List<Tuple2<Integer,Integer>> list = new
	 * ArrayList<Tuple2<Integer,Integer>>(); for (Tuple2<Integer,Integer> item :
	 * iterable) { list.add(item); } return list; }
	 */
	static Map<Integer, Double> iterableToHashMap(Iterable<Tuple2<Integer, Double>> iterable) {
		Map<Integer, Double> myMap = new HashMap<Integer, Double>();
		for (Tuple2<Integer, Double> item : iterable) {
			myMap.put(item._1, item._2);
		}
		return myMap;
	}

	static List<Long> mapListSNPs(Map<String, List<Long>> temp) {

		List<Long> longFullSNPList = new ArrayList<Long>();
		Set<Long> listSet = new HashSet<Long>();
		for (Map.Entry<String, List<Long>> ee : temp.entrySet()) {
			listSet.addAll(ee.getValue());
			// if (intFullSNPList.isEmpty()){
			// intFullSNPList.addAll(ee.getValue());
			// }
			// else{
			// intFullSNPList.retainAll(ee.getValue());
			// }
			// ListUtils.intersection(intFullSNPList, ee.getValue());

		}
		longFullSNPList.addAll(listSet);

		Collections.sort(longFullSNPList);

		return longFullSNPList;
	}

	static Map<String, Double> calculateScoreStatistics(Map<String, List<String>> GSubMap,
			Map<String, Double> SUM_U_Map) {

		Map<String, Double> GScoreMap = new HashMap<String, Double>();

		for (Map.Entry<String, List<String>> ee : GSubMap.entrySet()) {
			List<String> SNPList = ee.getValue();
			double val = 0;
			for (String SNP : SNPList) {

				val = val + SUM_U_Map.get(SNP);

			}

			GScoreMap.put(ee.getKey(), val);
			//System.out.println("SNP set ID: " + ee.getKey() + "  Score Statistics: " + val);

		}

		return GScoreMap;

	}

	static Map<String, List<String>> readGeneSubsets(String filename) {
		Map<String, List<String>> myMap = new HashMap<String, List<String>>();

		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(filename));
			String line = reader.readLine();
			while (line != null) {
				try {
					String[] tokens = line.split(","); // x,2 5
					List<String> stringList = new ArrayList<String>();

					String[] stringStringList;
					stringStringList = tokens[1].split(" ");

					for (String item : stringStringList) {
						stringList.add(item);
					}

					myMap.put(tokens[0], stringList);

				} catch (NumberFormatException ex) {
					System.out.println(ex.getMessage());
				}

				line = reader.readLine();
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		return myMap;
	}

	static Map<String, Boolean> createMapAllSNPs(Map<String, List<String>> geneSets) {
		Map<String, Boolean> allSNPs = new HashMap<String, Boolean>();

		for (Map.Entry<String, List<String>> ee : geneSets.entrySet()) {
			for (String element : ee.getValue()) {
				allSNPs.put(element, true);
			}
		}
		return allSNPs;
	}

	static Map<Integer, ArrayList<Double>> initMonteCarloMatrix(int permNum, int numPatients) {

		Random r = new Random();
		Map<Integer, ArrayList<Double>> map = new HashMap<Integer, ArrayList<Double>>();

		for (int i = 0; i < permNum; i++) {
			ArrayList<Double> doubleList = new ArrayList<Double>();
			for (int j = 0; j < numPatients; j++) {
				doubleList.add(r.nextGaussian());
			}
			map.put(i, doubleList);

		}
		return map;

	}

	
	static Map<Integer, Map<Integer, Double>> initScaramblingMatrix(int permNum, int numPatients, Map<Integer, Double> CurrxentEventTime) {
	
		
		Map<Integer, Map<Integer,  Double>> ScramblingMatrix = new HashMap<Integer, Map<Integer, Double>>();
		List<Double> vs =null;
		for(int i=0; i<permNum; i++){	
			Map<Integer, Double> x = new HashMap<Integer, Double>();
			if(vs==null)
				vs = new ArrayList<Double>(CurrxentEventTime.values());
			Collections.shuffle(vs);
			//System.out.println("vs Size: " + vs.size());
			final Iterator<Double> vIter = vs.iterator();
			for (Integer k : CurrxentEventTime.keySet()){
				Double element = vIter.next(); 
				x.put(k, element);
				if(DEBUG){				
					System.out.println("Scrambling => PID: " +  element);
				}
				
			}
			//System.out.println("X SIZE : " + x.size());
			ScramblingMatrix.put(i, x);
		}
		return ScramblingMatrix;
	}
	
	
	static Map<Integer, Map<Integer, Pair<Double, Integer>>> initScaramblingMatrix_COX(int permNum, int numPatients, Map<Integer, Pair<Double, Integer>> CurrxentEventTime) {
	
		//System.out.println("CurrxentEventTime size: " + CurrxentEventTime.size());

		
		Map<Integer, Map<Integer, Pair<Double, Integer>>> ScramblingMatrix = new HashMap<Integer, Map<Integer, Pair<Double, Integer>>>();
		List<Pair<Double, Integer>> vs =null;
		for(int i=0; i<permNum; i++){	
			Map<Integer, Pair<Double, Integer>> x = new HashMap<Integer, Pair<Double, Integer>>();
			if(vs==null)
				vs = new ArrayList<Pair<Double, Integer>>(CurrxentEventTime.values());
			Collections.shuffle(vs);
			//System.out.println("vs Size: " + vs.size());
			final Iterator<Pair<Double, Integer>> vIter = vs.iterator();
			for (Integer k : CurrxentEventTime.keySet()){
				Pair<Double, Integer> element = vIter.next(); 
				x.put(k, element);
				if(DEBUG){				
					System.out.println("Scrambling => PID: " +  element.getSurvivalTime());
				}
				
			}
			//System.out.println("X SIZE : " + x.size());
			ScramblingMatrix.put(i, x);
		}
		return ScramblingMatrix;
	}
	
	
	static Map<Integer, Pair<Double, Integer>> initTimeEvent(String Filename) throws FileNotFoundException {
		Scanner scanTimeEvent = new Scanner(new File(Filename));
		Map<Integer, Pair<Double, Integer>> map = new HashMap<Integer, Pair<Double, Integer>>();
		while (scanTimeEvent.hasNextLine()) {
			String s = scanTimeEvent.nextLine();
			String[] tokens = s.split(",");
			int PID = Integer.parseInt(tokens[0]);
			double Time = Double.parseDouble(tokens[1]);
			int Event = Integer.parseInt(tokens[2]);
			Pair<Double, Integer> TimeEventPair = new Pair<Double, Integer>(Time, Event);
			map.put(PID, TimeEventPair);
		}
		return map;
	}

	static Map<Integer, Double> initDoubleEvent(String Filename) throws FileNotFoundException {
		Scanner scanTimeEvent = new Scanner(new File(Filename));
		Map<Integer, Double> map = new HashMap<Integer, Double>();
		while (scanTimeEvent.hasNextLine()) {
			String s = scanTimeEvent.nextLine();
			String[] tokens = s.split(",");
			int PID = Integer.parseInt(tokens[0]);
			double Event = Double.parseDouble(tokens[1]);
			map.put(PID, Event);
		}
		return map;
	}

	static Map<Integer, Integer> initIntEvent(String Filename) throws FileNotFoundException {
		Scanner scanTimeEvent = new Scanner(new File(Filename));
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		while (scanTimeEvent.hasNextLine()) {
			String s = scanTimeEvent.nextLine();
			String[] tokens = s.split(",");
			int PID = Integer.parseInt(tokens[0]);
			int Event = Integer.parseInt(tokens[1]);
			map.put(PID, Event);
		}
		return map;
	}

}

