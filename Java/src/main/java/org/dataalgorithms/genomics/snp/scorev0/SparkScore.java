package org.dataalgorithms.genomics.snp.scorev0;

// STEP-0: import required Java/Spark classes.
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Iterator;
import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;

import scala.Tuple2;
import scala.Tuple3;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.*;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.mllib.linalg.distributed.BlockMatrix;
import org.apache.spark.mllib.linalg.distributed.CoordinateMatrix;
import org.apache.spark.mllib.linalg.distributed.IndexedRowMatrix;
import org.apache.spark.mllib.linalg.distributed.MatrixEntry;


public class SparkScore {

	public static int iter = 0;
	public static int numPermutation = 0;
	public static int numPatients = 0;
	public static boolean DEBUG = false;
	public static boolean CACHE = true;
	
	public static void main(String[] args) throws Exception {

        /*
         * Assumption: Genotype Matrix created based on Gene Subsets in
         * advance!!! Read input files
         *
         * 1) Genotype matrix (Column-based):
         *      KEY:SNP ID and VALUE: PID1,Val1 PID2,Val2 ... PIDn,Valn
         *      Format: SNPID&PID1,Val1 PID2,Val2 ... PIDn,Valn
         * 
         * 2) Pairs of Time and Event per patient; PID1,Time1,Event1, ..., PIDn,Timen,Eventn
         * HashMap for Cox: <PID, <Time,Event>>
         *         for Gaussian and Binomial: PID,EVENT {int, double}
         *
         *
         * 3) Type score=c("cox", "binomial", "gaussian")
         *
         *
         * 4) Gene Subsets GSubID SNPIDs $XYZ9 [1] "rs688" "rs469" "rs557"
         * "rs990" "rs346" "rs962" "rs501" "rs630" "rs913" "rs32" "rs802" <GSub
         * ID>, List of SNPs
         *
         * 5) Weights of SNPs - HashMap <SNPID, Weight>
         *
         * 6) [Optional] Number of Patients, Number of Iterations
         *     
         * 7) Type of Permutations: Scrambling or MonteCarlo
         *
         */

		// STEP-1: read input parameters and validate them
		if (args.length < 5) {
			System.err.println(
	  			"Usage: SparkSNP <Genotype Matrx ifilename> <Time Event Filename> <Function Type> <SNP-set Filename> <Num of Patients>,<Num of Permutations> <type of resampling>");
			System.exit(1);
		}

		String inputPathG = args[0];
		System.out.println("Gene Matrix inputPath: <file>=" + inputPathG);

		String inputPathTimeEvent = args[1];
		System.out.println("Time-Event inputPath: <file>=" + inputPathTimeEvent);

		String inputFunctionType = args[2];
		System.out.println("Input Function Type: cox, binomial , gaussian" + inputFunctionType);

		String inputPathGSubset = args[3];
		System.out.println("Gene Subset inputPath: <file>=" + inputPathGSubset);

		String inputPathWeight = args[4];
		System.out.println("Weight inputPath: <file>=" + inputPathWeight);

		// handle optional parameters:
		String inputTypePermutaion = "";
		String inputPathWG = "";
		if (args.length >= 6) {
			String[] tokens = args[5].split(",");
			numPermutation = Integer.parseInt(tokens[0]);
			numPatients = Integer.parseInt(tokens[1]);
			System.out.println("Number of Permutations: " + numPermutation);
			System.out.println("Number of Patients: " + numPatients);
		
			if (args.length >= 7) {
				inputTypePermutaion = args[6]; // Scrambling or MonteCarlo
				System.out.println("Permutation Type = " + inputTypePermutaion);
			}

			if (args.length >= 8) {
				inputPathWG = args[7];
				System.out.println("Patient Weight inputPath: <file>=" + inputPathWG);
			}
		}
		
		//U Matrix - Initialization
		JavaPairRDD<String, Tuple2<Integer, Double>> U = null;

		// STEP-2: Connect to the Sark master by creating JavaSparkContext object
		final JavaSparkContext ctx = new JavaSparkContext();

		// STEP-3: Read Genotype Matrix : input record format: SID&PID1,Val1
		// PID2,Val2 ... PIDn,Valn
		JavaRDD<String> GMlines = ctx.textFile(inputPathG, 5);

		// STEP-4: Read Gene Sets
		final Map<String, List<String>> GSubMap = readGeneSubsets(inputPathGSubset);

		// STEP-5: Create a set of all SNPs in geneSets
		final Map<String, Boolean> AllSNPs = createMapAllSNPs(GSubMap);

		// STEP-6.1: Filter out SNPs not participating in the calculation
		// STEP-6.2: Map the SNP sets after filtering
		JavaPairRDD<String, String> GMPairs	= filterAndMapSNPs(GMlines, AllSNPs);		
		
		if(DEBUG){
			System.out.println("=========GMPairs Filtering Test DEBUG=============");
			List<Tuple2<String, String>> debug1 = GMPairs.collect();
			for (Tuple2<String, String> t2 : debug1) {
				System.out.println("debug3 SNP ID=" + t2._1 + " Val: " + t2._2);
			}
		}
		
		// STEP-7: Read SNP Weights
		JavaRDD<String> SNPWeightlines = ctx.textFile(inputPathWeight, 1);
		JavaPairRDD<String, Double> SNPWeightPairs = buildSNPWeightPairs(SNPWeightlines, AllSNPs);		
		
		if(DEBUG){
			System.out.println("=========SNP Weight Pairs Test DEBUG=============");
			List<Tuple2<String, Double>> debugW = SNPWeightPairs.collect();
			for (Tuple2<String, Double> t2 : debugW) {
				System.out.println("debug3 SNP ID=" + t2._1 + " Weight: " + t2._2);
			}
		}
		
		// STEP-8:Score Statistics
		if (inputFunctionType.equals("cox")) {

			System.out.println("<==============COX=============>");

			// STEP-1: Pairs of <PID, <Time, Event>>
			final Broadcast<Map<Integer, Pair<Double, Integer>>> map = ctx.broadcast(initTimeEvent(inputPathTimeEvent));

			if(DEBUG){
				/* Debug Purpose */
				for (Map.Entry<Integer, Pair<Double, Integer>> entry : map.value().entrySet()) {
					System.out.println("Time:  " + entry.getValue().getSurvivalTime() + " Event: "
							+ entry.getValue().getEventIndicator());
				}
			}
			
			// STEP-2: Generate U[i, t] matrix
			JavaPairRDD<String, List<Tuple2<Integer, Double>>> U0 = generateGMPairs(GMPairs, map);
			
			if(DEBUG){
				System.out.println("========= U Matrix  Test DEBUG=============");
				List<Tuple2<String, List<Tuple2<Integer, Double>>>> debug2 = U0.collect();
				for (Tuple2<String, List<Tuple2<Integer, Double>>> t2 : debug2) {
					List<Tuple2<Integer, Double>> X = t2._2;
					for (Tuple2<Integer, Double> m : X)
						System.out.println("debug3 SNP ID=" + t2._1 + " PID: " + m._1 + " Val: " + m._2);
					System.out.println("debug3 SNP ID=" + t2._1 + " Size Val: " + t2._2.size());
				}
			}
	
			//Cache U0 for MonteCarlo Execution
			if (numPermutation > 0) 
				if (inputTypePermutaion.equals("MonteCarlo") && CACHE) {
					System.out.println("Cached U matrix!");
					U0.cache();
				}
			
			
			// STEP-3: Calculate Sigma of U[i, j] per each SNP <SNP_ID,
			// cox_value>
			JavaPairRDD<String, Double> sumU0 = U0
					.mapToPair(new PairFunction<Tuple2<String, List<Tuple2<Integer, Double>>>, String, Double>() {
						public Tuple2<String, Double> call(Tuple2<String, List<Tuple2<Integer, Double>>> s) {

							String SID = s._1;
							List<Tuple2<Integer, Double>> PID_Cox_VAl = s._2;

							double val_U = 0;
							for (Tuple2<Integer, Double> COX_VAL : PID_Cox_VAl) {
								double COX_val2 = COX_VAL._2;
								val_U = val_U + COX_val2;
							}
							return new Tuple2<String, Double>(SID, val_U);
						}
					});

			
			// STEP 4: Multiply SNP by their Weights
			JavaPairRDD<String, Tuple2<Double, Double>> joinOuterSigma = sumU0.join(SNPWeightPairs); //squareU0

			JavaPairRDD<String, Double> outerSigma = joinOuterSigma
					.mapToPair(new PairFunction<Tuple2<String, Tuple2<Double, Double>>, String, Double>() {
						public Tuple2<String, Double> call(Tuple2<String, Tuple2<Double, Double>> s) {

							return new Tuple2<String, Double>(s._1, (s._2._1*s._2._1) * s._2._2);
						}
					});
			
			if(DEBUG){
				System.out.println("========= Outer Sigma => COX per SNP  Test DEBUG=============");
				List<Tuple2<String, Double>> debug3 = outerSigma.collect();
				for (Tuple2<String, Double> t2 : debug3) {
					System.out.println("debug3 SNP ID=" + t2._1 + " COX Val: " + t2._2);
				}
			}

			// STEP-5-1: Calculate S0 per each GeneSet
			Map<String, Double> SUM_U_Map = outerSigma.collectAsMap();
			// STEP-5-2:
			final Map<String, Double> Score0Map = calculateScoreStatistics(GSubMap, SUM_U_Map);

			if(DEBUG){
				System.out.println("========= S0 - COX  DEBUG=============");
				for (Map.Entry<String, Double> entryS0 : Score0Map.entrySet()) {
					System.out.println("SNPSet ID=" + entryS0.getKey() + " COX Val: " + entryS0.getValue());
				}
			}
			// <--------------------------------------PERMUTATION------------------------------------------------>//
			if (numPermutation > 0) {
				
				System.out.println("========= Permutation Sampling =============");


				if (inputTypePermutaion.equals("Scrambling")) {
					// Solution1: Scrambling
					System.out.println("========= Scrambling =============");

					//STEP-1: Scambling Initialization 
					final Broadcast<Map<Integer, Map<Integer,Pair<Double,Integer>>>> ScramblingSampling = ctx
							.broadcast(initScaramblingMatrix(numPermutation, numPatients,  map.value() )); //

				/*	for (final Map.Entry<Integer, Map<Integer,Pair<Double,Integer>>>> entryPerm : ScramblingSampling.value()
							.entrySet()) {
						System.out.println(
								"Permutation ID=" + entryPerm.getKey() + " Val Size: " + entryPerm.getValue().size());

						ArrayList<Double> X = entryPerm.getValue();
						for (Double tmp : X)
							System.out.println("Permutation ID=" + entryPerm.getKey() + " Val: " + tmp);
					}
					*/
					Map<String, Long> incrementCounters = new HashMap<String, Long>();
					
					while (iter < numPermutation) {
						
						// STEP-2: Generate U_Si[i, t] matrix
						JavaPairRDD<String, List<Tuple2<Integer, Double>>> U_Si = GMPairs
								.mapToPair(new PairFunction<Tuple2<String, String>, String, List<Tuple2<Integer, Double>>>() {
									public Tuple2<String, List<Tuple2<Integer, Double>>> call(Tuple2<String, String> s) {

										String SID = s._1; // SNP ID
										String[] tokens = s._2.split(" "); // PID1,Val1
																			// PID2,Val2
										
										//Retrive the corresponding scrambled TimeEvent values	
										Map<Integer, Pair<Double,Integer>> iterMapTimeEvent = ScramblingSampling.value().get(iter);
										
										Map<Integer, Double> SNPMapVal = new HashMap<Integer, Double>();
										Map<Integer, Double> SNPMapPartialSum = new HashMap<Integer, Double>();
										List<Tuple2<Integer, Double>> SNP_COX_Elem = new ArrayList<Tuple2<Integer, Double>>();

										// Create a map of <PID, Val> for the assigned SNP
										for (String tempS : tokens) {
											String[] tempPIDsVals = tempS.split(",");
											int PID = Integer.parseInt(tempPIDsVals[0]);
											double val = Double.parseDouble(tempPIDsVals[1]);
											SNPMapVal.put(PID, val);
										}

										for (Map.Entry<Integer, Double> mainEntry : SNPMapVal.entrySet()) {

											int PID = mainEntry.getKey();
											double Gij = mainEntry.getValue();

											double Yi = iterMapTimeEvent.get(PID).getSurvivalTime();
											int Delta_i = iterMapTimeEvent.get(PID).getEventIndicator();

											double a_ji = 0;
											double b_i = 0;

											for (Map.Entry<Integer, Pair<Double, Integer>> entry : iterMapTimeEvent.entrySet()) {

												if (Yi <= entry.getValue().getSurvivalTime()) {
													a_ji = a_ji + SNPMapVal.get(entry.getKey());
													// PID: map.value().get(entry.getKey());
													b_i = b_i + 1; // entry.getValue().getSurvivalTime();
												}
											}

											double coxVal = Delta_i * (Gij - (a_ji / b_i));
											// SNPMapPartialSum.put(PID, coxVal);
											Tuple2<Integer, Double> PID_Cox_VAl = new Tuple2<Integer, Double>(PID, coxVal);
											SNP_COX_Elem.add(PID_Cox_VAl);
										}

										/*
										 * double sum =0; for (Map.Entry<Integer, Double>
										 * entry : SNPMapPartialSum.entrySet()) sum = sum +
										 * entry.getValue();
										 */
										return new Tuple2<String, List<Tuple2<Integer, Double>>>(SID, SNP_COX_Elem);
									}
								});

						if(DEBUG){
							System.out.println("========= U Matrix  Test DEBUG=============");
							List<Tuple2<String, List<Tuple2<Integer, Double>>>> debug2i = U_Si.collect();
							for (Tuple2<String, List<Tuple2<Integer, Double>>> t2 : debug2i) {
								List<Tuple2<Integer, Double>> X = t2._2;
								for (Tuple2<Integer, Double> m : X)
									System.out.println("debug3 SNP ID=" + t2._1 + " PID: " + m._1 + " Val: " + m._2);
								System.out.println("debug3 SNP ID=" + t2._1 + " Size Val: " + t2._2.size());
							}
						}
						// STEP-3: Calculate Sigma of U[i, j] per each SNP <SNP_ID,
						// cox_value>
						JavaPairRDD<String, Double> sumU_Si = U_Si
								.mapToPair(new PairFunction<Tuple2<String, List<Tuple2<Integer, Double>>>, String, Double>() {
									public Tuple2<String, Double> call(Tuple2<String, List<Tuple2<Integer, Double>>> s) {

										String SID = s._1;
										List<Tuple2<Integer, Double>> PID_Cox_VAl = s._2;

										double val_U = 0;
										for (Tuple2<Integer, Double> COX_VAL : PID_Cox_VAl) {
											double COX_val2 = COX_VAL._2;
											val_U = val_U + COX_val2;
										}
										return new Tuple2<String, Double>(SID, val_U);
									}
								});

						
						// STEP 4: Multiply SNP by their Weights
						JavaPairRDD<String, Tuple2<Double, Double>> joinOuterSigma_Si = sumU_Si // squareU_Si
								.join(SNPWeightPairs);

						// STEP 5: SNP Score
						JavaPairRDD<String, Double> outerSigma_Si = joinOuterSigma_Si
								.mapToPair(new PairFunction<Tuple2<String, Tuple2<Double, Double>>, String, Double>() {
									public Tuple2<String, Double> call(Tuple2<String, Tuple2<Double, Double>> s) {

										return new Tuple2<String, Double>(s._1, (s._2._1*s._2._1) * s._2._2);
									}
								});

						// STEP 6: Collect SNP Scores as a map
						Map<String, Double> SUM_U_Map_Si = outerSigma_Si.collectAsMap();
						
						// STEP 7: Calculate S_{k}
						Map<String, Double> ScoreSiMap = calculateScoreStatistics(GSubMap, SUM_U_Map_Si);

						// STEP 8: Calculate counter_{k}
						for (Map.Entry<String, Double> entryInc : ScoreSiMap.entrySet()) {

							if (entryInc.getValue() >= Score0Map.get(entryInc.getKey())) {
								Long count = incrementCounters.get(entryInc.getKey());
								if (count == null) {
									incrementCounters.put(entryInc.getKey(), (long) 1);
								} else {
									incrementCounters.put(entryInc.getKey(), count + 1);
								}
							}
						}
						iter++;
					}
					if(DEBUG){
						for (Map.Entry<String, Long> entryInc : incrementCounters.entrySet()) {
							System.out.println("SNPSet ID=" + entryInc.getKey() + " Inc Value: " + entryInc.getValue());
						}
					}
					
				} else if (inputTypePermutaion.equals("MonteCarlo")) {
					// Solution2: MonteCarlo
					System.out.println("========= MonteCarlo =============");

					// Due to space required for U[i, t] matrix per each
					// iteration, we didn't parllelize it at the loop level
					// We have tested for a smaller granularity at U[i,t] and that caused 
					// lots of execution overhead due to the huge number of map tasks
					// Therefore, the granularity is at the column level
					final Broadcast<Map<Integer, ArrayList<Double>>> MonteCarloPermutations = ctx
							.broadcast(initMonteCarloMatrix(numPermutation, numPatients)); //
					
					if(DEBUG){
						for (final Map.Entry<Integer, ArrayList<Double>> entryPerm : MonteCarloPermutations.value()
								.entrySet()) {
							System.out.println(
									"Permutation ID=" + entryPerm.getKey() + " Val Size: " + entryPerm.getValue().size());
	
							ArrayList<Double> X = entryPerm.getValue();
							for (Double tmp : X)
								System.out.println("Permutation ID=" + entryPerm.getKey() + " Val: " + tmp);
						}
					}
					
					Map<String, Long> incrementCounters = new HashMap<String, Long>();

					while (iter < numPermutation) {
						// STEP-1: Calculate (weight*U[i, j]) per each SNP
						JavaPairRDD<String, Double> sumU_Si = U0.mapToPair(
								new PairFunction<Tuple2<String, List<Tuple2<Integer, Double>>>, String, Double>() {
									public Tuple2<String, Double> call(
											Tuple2<String, List<Tuple2<Integer, Double>>> s) {
										final ArrayList<Double> currentPermutationWeights = new ArrayList(
												MonteCarloPermutations.value().get(iter));

										String SID = s._1;
										List<Tuple2<Integer, Double>> PID_Cox_VAl = s._2;
										double val_U = 0;
										for (Tuple2<Integer, Double> COX_VAL : PID_Cox_VAl) {
											int PID = COX_VAL._1; // PID starts from 1 to n
											double COX_val2 = COX_VAL._2;
											// Monte Carlo Weight (PID-1 strats from 0 to n-1
											COX_val2 = currentPermutationWeights.get(PID - 1) * COX_val2;
											val_U = val_U + COX_val2;
										}
										return new Tuple2<String, Double>(SID, val_U);
									}
								});

						// STEP 2: calculate Score per each SNP by joining SumU_Si and WeightRDD Multiply SNP by their Weights
						JavaPairRDD<String, Tuple2<Double, Double>> joinOuterSigma_Si = sumU_Si.join(SNPWeightPairs);
						JavaPairRDD<String, Double> outerSigma_Si = joinOuterSigma_Si
								.mapToPair(new PairFunction<Tuple2<String, Tuple2<Double, Double>>, String, Double>() {
									public Tuple2<String, Double> call(Tuple2<String, Tuple2<Double, Double>> s) {

										return new Tuple2<String, Double>(s._1, (s._2._1 * s._2._1) * s._2._2);
									}
								});

						// STEP 3: Collect SNP Score as a map
						Map<String, Double> SUM_U_Map_Si = outerSigma_Si.collectAsMap();
						// STEP 4: S_{k} 
						Map<String, Double> ScoreSiMap = calculateScoreStatistics(GSubMap, SUM_U_Map_Si);
						// STEP 5: Counter_{k} 
						for (Map.Entry<String, Double> entryInc : ScoreSiMap.entrySet()) {

							if (entryInc.getValue() >= Score0Map.get(entryInc.getKey())) {
								Long count = incrementCounters.get(entryInc.getKey());
								if (count == null) {
									incrementCounters.put(entryInc.getKey(), (long) 1);
								} else {
									incrementCounters.put(entryInc.getKey(), count + 1);
								}
							}
						}
						iter++;
					}
					if(DEBUG){	
						for (Map.Entry<String, Long> entryInc : incrementCounters.entrySet()) {
							System.out.println("SNPSet ID=" + entryInc.getKey() + " Inc Value: " + entryInc.getValue());
						}
					}

				} // end of MonteCarlo
			} // end of Permutations

		} else { // [Binomial and Gaussian]

			// STEP-0: calculate mean values of columns - First group by SNP_ID
			// [Binomial and Gaussian]
			// **************** <Potential Point for Improvement - In case we
			// have a better way of taking mean - Java 8> ********************
			// JavaPairRDD<String, Double> MeanGM = GropByKeyGM.mapToPair(
			// new PairFunction<Tuple2<String, Iterable<Tuple2<Integer,
			// Double>>>, String, Double>() {
			// public Tuple2<String, Double> call(Tuple2<String,
			// Iterable<Tuple2<Integer, Double>>> s) {
			//
			// String SID =s._1;
			// double sum =0;
			// int cnt=0;
			//
			// for (Tuple2<Integer, Double> item : s._2) {
			// sum = sum + item._2;
			// cnt++;
			// }
			//
			// double mean = sum / cnt;
			//
			// return new Tuple2<String, Double>(SID, mean);
			// }
			// });

			if (inputFunctionType.equals("binomial")) {
				System.out.println("=========Binomial=============");

				// //STEP-1: Read Event (Integer) vector (PID, Event_val)
				// JavaRDD<String> eventRDD = ctx.textFile(inputPathTimeEvent,
				// 1);
				// JavaPairRDD<Integer, Integer> eventPairs =
				// eventRDD.mapToPair(
				// new PairFunction<String, Integer, Integer>() {
				// public Tuple2<Integer, Integer> call(String s) {
				// String[] tokens = s.split(","); // PID,24
				// int PID = Integer.parseInt(tokens[0]);
				// int Event = Integer.parseInt(tokens[1]);
				// return new Tuple2<Integer, Integer>(PID, Event);
				// }
				// });
				//
				// //STEP-2: Calculate mean value of Event vector and broadcast
				// it
				// final Broadcast<Double> YMean = ctx.broadcast(50.02);
				// //eventPairs.mapToDouble(tuple -> tuple._2).mean());
				// //System.out.println("Y Mean Value: " + YMean.value());
				//
				// //STEP-3: Read Event vector as a map <PID, Event_val>, and
				// broadcast it
				// final Broadcast<Map<Integer, Integer>> map =
				// ctx.broadcast(initIntEvent(inputPathTimeEvent));
				//
				// //STEP-4: Join Genotype Matrix w/ Mean of Gis -> or add mean
				// Gi to the item
				// JavaPairRDD<String, Tuple2<Tuple2<Integer, Double>, Double>>
				// inputParamsGM = GMPairs.join(MeanGM);
				//
				// //STEP-5: Calculate U[i, t] -> <SID, <<PID, val>, Mean
				// G_SID>>
				// U = inputParamsGM.mapToPair(
				// new PairFunction<Tuple2<String, Tuple2<Tuple2<Integer,
				// Double>, Double>>, String, Tuple2<Integer, Double>>() {
				// public Tuple2<String, Tuple2<Integer, Double>>
				// call(Tuple2<String, Tuple2<Tuple2<Integer, Double>, Double>>
				// s) {
				//
				// String SID =s._1;
				// Tuple2<Tuple2<Integer, Double>, Double> PID_Val_cGbar =
				// (Tuple2<Tuple2<Integer, Double>, Double>) s._2;
				// Tuple2<Integer, Double> PID_Val = (Tuple2<Integer, Double>)
				// PID_Val_cGbar._1;
				// int PID = PID_Val._1;
				// double Gij = PID_Val._2;
				// double GiMean = PID_Val_cGbar._2;
				// int Yi = map.value().get(PID);
				//
				// double binoVal= (Yi - YMean.value()) * (Gij - GiMean);
				//
				// Tuple2<Integer, Double> PID_U = new Tuple2<Integer,
				// Double>(PID, binoVal);
				// return new Tuple2<String, Tuple2<Integer, Double>>(SID,
				// PID_U);
				// }
				// });
				//
				// System.out.println("=========Binomial U DEBUG=============");
				// List<Tuple2<String, Tuple2<Integer, Double>>> debug6 =
				// U.collect();
				// for (Tuple2<String, Tuple2<Integer, Double>> t2 : debug6) {
				//
				// Tuple2<Integer, Double> x = (Tuple2<Integer, Double>) t2._2;
				// System.out.println("debug3 key SNP ID=" + t2._1 + " PID: " +
				// x._1 + " U[ "+ t2._1+ ", " + x._1 +" ] = " + x._2);
				//
				// }

			} else if (inputFunctionType.equals("gaussian")) {
				System.out.println("=========Gaussian=============");

				// //STEP-1: Read Event (Double) vector (PID, Event_val)
				// JavaRDD<String> eventRDD = ctx.textFile(inputPathTimeEvent,
				// 1);
				// JavaPairRDD<Integer, Double> eventPairs = eventRDD.mapToPair(
				// new PairFunction<String, Integer, Double>() {
				// public Tuple2<Integer, Double> call(String s) {
				// String[] tokens = s.split(","); // PID,24
				// int PID = Integer.parseInt(tokens[0]);
				// double Event = Double.parseDouble(tokens[1]);
				// return new Tuple2<Integer, Double>(PID, Event);
				// }
				// });
				//
				// //STEP-2: Calculate mean value of Event vector and broadcast
				// it
				// final Broadcast<Double> YMean = ctx.broadcast(50.02);
				// //eventPairs.mapToDouble(tuple -> tuple._2).mean());
				// //System.out.println("Y Mean Value: " + YMean.value());
				//
				// //STEP-3: Read Event vector as a map <PID, Event_val>, and
				// broadcast it
				// final Broadcast<Map<Integer, Double>> map =
				// ctx.broadcast(initDoubleEvent(inputPathTimeEvent));
				//
				// //STEP-4: Join Genotype Matrix w/ Mean of Gis -> or add mean
				// Gi to the item
				// JavaPairRDD<String, Tuple2<Tuple2<Integer, Double>, Double>>
				// inputParamsGM = GMPairs.join(MeanGM);
				//
				// //STEP-5: Calculate U[i, t] -> <SID, <<PID, val>, Mean
				// G_SID>>
				// U = inputParamsGM.mapToPair(
				// new PairFunction<Tuple2<String, Tuple2<Tuple2<Integer,
				// Double>, Double>>, String, Tuple2<Integer, Double>>() {
				// public Tuple2<String, Tuple2<Integer, Double>>
				// call(Tuple2<String, Tuple2<Tuple2<Integer, Double>, Double>>
				// s) {
				//
				// String SID =s._1;
				// Tuple2<Tuple2<Integer, Double>, Double> PID_Val_cGbar =
				// (Tuple2<Tuple2<Integer, Double>, Double>) s._2;
				// Tuple2<Integer, Double> PID_Val = (Tuple2<Integer, Double>)
				// PID_Val_cGbar._1;
				// int PID = PID_Val._1;
				// double Gij = PID_Val._2;
				// double GiMean = PID_Val_cGbar._2;
				// double Yi = map.value().get(PID);
				//
				// double gaussianVal= (Yi - YMean.value()) * (Gij - GiMean);
				//
				// Tuple2<Integer, Double> PID_U = new Tuple2<Integer,
				// Double>(PID, gaussianVal);
				// return new Tuple2<String, Tuple2<Integer, Double>>(SID,
				// PID_U);
				// }
				// });
				//
				// System.out.println("=========Gaussian U DEBUG=============");
				// List<Tuple2<String, Tuple2<Integer, Double>>> debug6 =
				// U.collect();
				// for (Tuple2<String, Tuple2<Integer, Double>> t2 : debug6) {
				// Tuple2<Integer, Double> x = (Tuple2<Integer, Double>) t2._2;
				// System.out.println("debug3 key SNP ID=" + t2._1 + " PID: " +
				// x._1 + " U[ "+ t2._1+ ", " + x._1 +" ] = " + x._2);
				// }

			}

		} // end of [Binomial/Gaussian]

		// **************** <Potential Point for Improvement - GroupByKey to
		// CombineByKey > ********************
		// JavaPairRDD<String, Iterable<Tuple2<Integer, Double>>> GropByKeyU =
		// U.groupByKey();

		// **************** <Potential Point for Improvement - In case we have a
		// better way of summatiuon - Reduction - Java 8> ********************
		// JavaPairRDD<String, Double> SUM_U = GropByKeyU.mapToPair(
		// new PairFunction<Tuple2<String, Iterable<Tuple2<Integer, Double>>>,
		// String, Double>() {
		// public Tuple2<String, Double> call(Tuple2<String,
		// Iterable<Tuple2<Integer, Double>>> s) {
		//
		// String SID =s._1;
		// double sum =0;
		//
		// for (Tuple2<Integer, Double> item : s._2) {
		// sum = sum + item._2;
		// }
		//
		// return new Tuple2<String, Double>(SID, sum);
		// }
		// });

		// long estimatedTime = System.currentTimeMillis() - startTime;

		System.out.println("=========Final=============");

		// //Create HashMap
		// //collectAsMap at SparkSNP.java:461, took 2737.236938 s
		// //SparkSNP.java:464, took 298.468870 s
		// Map<String, Double> SUM_U_Map = SUM_U.collectAsMap(); // new
		// HashMap<String, Double>();
		// //System.out.println("=========Time Elapsed=============" +
		// estimatedTime);
		//
		//

		/*
		 * // STEP-4: Read Gene Subsets // JavaRDD<String> GSubListlines =
		 * ctx.textFile(inputPathGSubset, 1); Map<String, List<String>> GSubMap
		 * = readGeneSubsets(inputPathGSubset); Map<String, Double> GScoreMap =
		 * calculateScoreStatistics(GSubMap, SUM_U_Map);
		 */

		// STEP-8: SUM[U[i, t]]
		// groupByKey
		// JavaPairRDD<Long, Iterable<Tuple<Integer, Double>>> U.groupByKey();
		// mapToPaired => SNP_ID, SUM U[i, t] AND SQURE
		// JavaPairRDD<String, Double> SUM2_U =

		// JavaPairRDD<String, Double> SUM2_U = U.reduceByKey(new
		// Function2<Tuple2<Integer, Double>, Tuple2<Integer, Double>, Double>()
		// {
		// public Double call(Tuple2<Integer, Double> i1, Tuple2<Integer,
		// Double> i2) {
		// return i1._2 + i2._2;
		// }
		// });

		// U.reduceByKey((a, b) -> a._2._2 + b._2._2);

		// Join SUM2_U AND Weight => <SNP, <SUM2, W>
		// JavaPairRDD<Long, Tuple2<Double, Double>> SUM2_U_W_Pairs =
		// SUM2_U.join(weightPairs);

		// MapToPaired => SUM2_U_W_Pairs => W * SUM2
		// JavaPairRDD<Long, Double> SNP_Score;

		// Create a HashMap AND calculate S_k

		ctx.stop();
		System.exit(0);
	}

	/*
	double calcMean(Map<Integer, Integer> map) {

		long Sum = 0;
		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			Sum = Sum + entry.getValue();
		}
		double mean = Sum / map.size();
		return mean;
	}
	*/

	static JavaPairRDD<String, List<Double>> createPairRDD(JavaRDD lines, final List<Integer> indexes) {
		JavaPairRDD<String, List<Double>> pairs = lines.mapToPair(new PairFunction<String, String, List<Double>>() {
			public Tuple2<String, List<Double>> call(String s) {
				String[] tokens = s.split(","); // PID,0.2 15.2 -3.76
				List<Double> doubleList = new ArrayList<Double>();
				String[] stringDoubleList;
				stringDoubleList = tokens[1].split(" ");
				
				if(DEBUG){
					System.out.println(tokens[0] + "," + tokens[1]);
					System.out.println("<========= " + stringDoubleList.length + "=========>");
				}

				int index = 0;
				int indexGSub = 0;
				for (Integer item : indexes) {
					// while(index != indexes.get(indexGSub)){
					// index++;
					// }
					if(DEBUG){
						System.out.println("Index -> " + item);
					}
					doubleList.add(Double.parseDouble(stringDoubleList[item]));
					// indexGSub++;
					// if (indexes.size()>1)
					// indexes.remove(0);
				}

				// PID , <0.2 15.2 -3.76>
				return new Tuple2<String, List<Double>>(tokens[0], doubleList);
			}
		});

		return pairs;

	}

	/*
	 * static List<Tuple2<Integer,Integer>>
	 * iterableToList(Iterable<Tuple2<Integer,Integer>> iterable) {
	 * List<Tuple2<Integer,Integer>> list = new
	 * ArrayList<Tuple2<Integer,Integer>>(); for (Tuple2<Integer,Integer> item :
	 * iterable) { list.add(item); } return list; }
	 */
	static Map<Integer, Double> iterableToHashMap(Iterable<Tuple2<Integer, Double>> iterable) {
		Map<Integer, Double> myMap = new HashMap<Integer, Double>();
		for (Tuple2<Integer, Double> item : iterable) {
			myMap.put(item._1, item._2);
		}
		return myMap;
	}

	static List<Long> mapListSNPs(Map<String, List<Long>> temp) {
		List<Long> longFullSNPList = new ArrayList<Long>();
		Set<Long> listSet = new HashSet<Long>();
		for (Map.Entry<String, List<Long>> ee : temp.entrySet()) {
			listSet.addAll(ee.getValue());
			// if (intFullSNPList.isEmpty()){
			// intFullSNPList.addAll(ee.getValue());
			// }
			// else{
			// intFullSNPList.retainAll(ee.getValue());
			// }
			// ListUtils.intersection(intFullSNPList, ee.getValue());
		}
		longFullSNPList.addAll(listSet);
		//
		Collections.sort(longFullSNPList);
		return longFullSNPList;
	}

	static Map<String, Double> calculateScoreStatistics(
			Map<String, List<String>> GSubMap,
			Map<String, Double> SUM_U_Map) {

		Map<String, Double> GScoreMap = new HashMap<String, Double>();
		//
		for (Map.Entry<String, List<String>> ee : GSubMap.entrySet()) {
			List<String> SNPList = ee.getValue();
			double val = 0;
			for (String SNP : SNPList) {
				val = val + SUM_U_Map.get(SNP);
			}
			//
			GScoreMap.put(ee.getKey(), val);
			//System.out.println("SNP set ID: " + ee.getKey() + "  Score Statistics: " + val);
		}
		return GScoreMap;
	}

	static Map<String, List<String>> readGeneSubsets(String filename) throws Exception {
		Map<String, List<String>> myMap = new HashMap<String, List<String>>();
		//
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(filename));
			String line = reader.readLine();
			while (line != null) {
				String[] tokens = line.split(","); // x,2 5
				List<String> stringList = new ArrayList<String>();
				//
				String[] stringStringList = tokens[1].split(" ");
				for (String item : stringStringList) {
						stringList.add(item);
				}
				myMap.put(tokens[0], stringList);
				line = reader.readLine();
			}
		} 
		finally {
			if (reader != null) {
				reader.close();
			}
		}
		//
		return myMap;
	}

	static Map<String, Boolean> createMapAllSNPs(Map<String, List<String>> geneSets) {
		Map<String, Boolean> allSNPs = new HashMap<String, Boolean>();
		for (Map.Entry<String, List<String>> ee : geneSets.entrySet()) {
			for (String element : ee.getValue()) {
				allSNPs.put(element, true);
			}
		}
		return allSNPs;
	}

	static Map<Integer, ArrayList<Double>> initMonteCarloMatrix(int permNum, int numPatients) {
		Random r = new Random();
		Map<Integer, ArrayList<Double>> map = new HashMap<Integer, ArrayList<Double>>();
		//
		for (int i = 0; i < permNum; i++) {
			ArrayList<Double> doubleList = new ArrayList<Double>();
			for (int j = 0; j < numPatients; j++) {
				doubleList.add(r.nextGaussian());
			}
			map.put(i, doubleList);
		}
		//
		return map;
	}

	
	static Map<Integer, Map<Integer, Pair<Double, Integer>>> initScaramblingMatrix(
			int permNum, 
			int numPatients, 
			Map<Integer, Pair<Double, Integer>> CurrxentEventTime) {
	
		//System.out.println("CurrxentEventTime size: " + CurrxentEventTime.size());
	
		Map<Integer, Map<Integer, Pair<Double, Integer>>> ScramblingMatrix = new HashMap<Integer, Map<Integer, Pair<Double, Integer>>>();
		List<Pair<Double, Integer>> vs =null;
		for(int i=0; i<permNum; i++){	
			Map<Integer, Pair<Double, Integer>> x = new HashMap<Integer, Pair<Double, Integer>>();
			if(vs == null) {
				vs = new ArrayList<Pair<Double, Integer>>(CurrxentEventTime.values());
			}
			Collections.shuffle(vs);
			//System.out.println("vs Size: " + vs.size());
			final Iterator<Pair<Double, Integer>> vIter = vs.iterator();
			for (Integer k : CurrxentEventTime.keySet()){
				Pair<Double, Integer> element = vIter.next(); 
				x.put(k, element);
				if(DEBUG){				
					System.out.println("Scrambling => PID: " +  element.getSurvivalTime());
				}
			}
			//System.out.println("X SIZE : " + x.size());
			ScramblingMatrix.put(i, x);
		}
		return ScramblingMatrix;
	}
	
	
	static Map<Integer, Pair<Double, Integer>> initTimeEvent(String Filename) throws FileNotFoundException {
		Scanner scanTimeEvent = new Scanner(new File(Filename));
		Map<Integer, Pair<Double, Integer>> map = new HashMap<Integer, Pair<Double, Integer>>();
		while (scanTimeEvent.hasNextLine()) {
			String s = scanTimeEvent.nextLine();
			String[] tokens = s.split(",");
			int PID = Integer.parseInt(tokens[0]);
			double Time = Double.parseDouble(tokens[1]);
			int Event = Integer.parseInt(tokens[2]);
			Pair<Double, Integer> TimeEventPair = new Pair<Double, Integer>(Time, Event);
			map.put(PID, TimeEventPair);
		}
		return map;
	}

	static Map<Integer, Double> initDoubleEvent(String Filename) throws FileNotFoundException {
		Scanner scanTimeEvent = new Scanner(new File(Filename));
		Map<Integer, Double> map = new HashMap<Integer, Double>();
		while (scanTimeEvent.hasNextLine()) {
			String s = scanTimeEvent.nextLine();
			String[] tokens = s.split(",");
			int PID = Integer.parseInt(tokens[0]);
			double Event = Double.parseDouble(tokens[1]);
			map.put(PID, Event);
		}
		return map;
	}

	static Map<Integer, Integer> initIntEvent(String Filename) throws FileNotFoundException {
		Scanner scanTimeEvent = new Scanner(new File(Filename));
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		while (scanTimeEvent.hasNextLine()) {
			String s = scanTimeEvent.nextLine();
			String[] tokens = s.split(",");
			int PID = Integer.parseInt(tokens[0]);
			int Event = Integer.parseInt(tokens[1]);
			map.put(PID, Event);
		}
		return map;
	}
	

	// STEP-6.1: Filter out SNPs not participating in the calculation
	// STEP-6.2: Map the SNP sets after filtering
	static JavaPairRDD<String, String> filterAndMapSNPs(
		final JavaRDD<String> GMlines, 
		final Map<String, Boolean> AllSNPs) {
		return 	GMlines	
				  //Worst Case is to compute all of them 
				.filter(new Function<String, Boolean>() { 
					public Boolean call(String s) {
						String[] tokens = s.split("&"); // SID&PID1,Val1 PID2,Val2
				  		String SNPID = tokens[0]; String PIDsVals = tokens[1]; Object
				  		obj = AllSNPs.get(SNPID); 
				  		if(obj != null) {
				  			return true; 
				  		}
				  		else {
				  			return false;
				  		}
				  	 } 
				})
				.mapToPair(new PairFunction<String, String, String>() {
					public Tuple2<String, String> call(String s) {
						String[] tokens = s.split("&"); // SID&PID1,Val1 PID2,Val2 .. PID_n,Val_n
						String SNPID = tokens[0];
						String PIDsVals = tokens[1];
						return new Tuple2<String, String>(SNPID, PIDsVals);
					}
				});	
	}
	
	
	static JavaPairRDD<String, Double> buildSNPWeightPairs(
		final JavaRDD<String> SNPWeightlines, 
		final Map<String, Boolean> AllSNPs) {
		//
		return SNPWeightlines
		.filter(new Function<String, Boolean>() {
			public Boolean call(String s) {
				String[] tokens = s.split(" ");
				String SNPID = tokens[0];
				Object obj = AllSNPs.get(SNPID);
				if (obj != null)
					return true;
				else
					return false;
			}
		}).mapToPair(new PairFunction<String, String, Double>() {
			public Tuple2<String, Double> call(String s) {
				String[] tokens = s.split(" "); // <KEY:SNP_ID VALUE:Weight>
				String SNPID = tokens[0];
				double weight = Double.parseDouble(tokens[1]);
				double squreW = weight*weight; 
				return new Tuple2<String, Double>(SNPID, squreW);
			}
		});
	}


	// STEP-2: Generate U[i, t] matrix
	static JavaPairRDD<String, List<Tuple2<Integer, Double>>> generateGMPairs(
			JavaPairRDD<String, String> GMPairs,
			final Broadcast<Map<Integer, Pair<Double, Integer>>> map) {
		//
		return
		GMPairs.mapToPair(new PairFunction<Tuple2<String, String>, String, List<Tuple2<Integer, Double>>>() {
				public Tuple2<String, List<Tuple2<Integer, Double>>> call(Tuple2<String, String> s) {
					String SID = s._1; // SNP ID
					String[] tokens = s._2.split(" "); // PID1,Val1

					Map<Integer, Double> SNPMapVal = new HashMap<Integer, Double>();
					Map<Integer, Double> SNPMapPartialSum = new HashMap<Integer, Double>();
					List<Tuple2<Integer, Double>> SNP_COX_Elem = new ArrayList<Tuple2<Integer, Double>>();

					// Create a map of <PID, Val> for the assigned SNP
					for (String tempS : tokens) {
						String[] tempPIDsVals = tempS.split(",");
						int PID = Integer.parseInt(tempPIDsVals[0]);
						double val = Double.parseDouble(tempPIDsVals[1]);
						SNPMapVal.put(PID, val);
					}
					// Potential optimization => Comparsion Matrix
					for (Map.Entry<Integer, Double> mainEntry : SNPMapVal.entrySet()) {

						int PID = mainEntry.getKey();
						double Gij = mainEntry.getValue();

						double Yi = map.value().get(PID).getSurvivalTime();
						int Delta_i = map.value().get(PID).getEventIndicator();

						double a_ji = 0;
						double b_i = 0;

						for (Map.Entry<Integer, Pair<Double, Integer>> entry : map.value().entrySet()) {

							if (Yi <= entry.getValue().getSurvivalTime()) {
								a_ji = a_ji + SNPMapVal.get(entry.getKey());
								b_i = b_i + 1; 
							}
						}

						double coxVal = Delta_i * (Gij - (a_ji / b_i));
						Tuple2<Integer, Double> PID_Cox_VAl = new Tuple2<Integer, Double>(PID, coxVal);
						SNP_COX_Elem.add(PID_Cox_VAl);
					}

					return new Tuple2<String, List<Tuple2<Integer, Double>>>(SID, SNP_COX_Elem);
				}
			});
		}	
	
}
